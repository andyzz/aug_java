package com.example.talent.aug;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SolutionTests {

    @Test
    void test_regex() {
        Pattern pattern = Pattern.compile("((\\(\\d*\\))+)$");
        Matcher m = pattern.matcher("abc(1)(1)(2)");
        if (m.find()) {
            assertEquals(2, m.groupCount());
            assertEquals("(1)(1)(2)", m.group(0));
            assertEquals("(1)(1)(2)", m.group(1));
            assertEquals("(2)", m.group(2));
        }
        assertEquals("abc", m.replaceAll(""));
    }

    @Test
    @DisplayName("Rename 01")
    void rename_01() {
        Solution solution = new Solution();
        String[] expected = {"pes", "fifa", "gta", "pes(2019)"};
        String[] names = {"pes", "fifa", "gta", "pes(2019)"};
        assertArrayEquals(expected, solution.rename(names), "英文测试01");
    }

    @Test
    @DisplayName("Rename 02")
    void rename_02() {
        Solution solution = new Solution();
        String[] expected = {"gta", "gta(1)", "gta(2)", "avalon"};
        String[] names = {"gta", "gta(1)", "gta", "avalon"};
        assertArrayEquals(expected, solution.rename(names), "英文测试02");
    }

    @Test
    @DisplayName("Rename 03")
    void rename_03() {
        Solution solution = new Solution();
        String[] expected = {"onepiece", "onepiece(1)", "onepiece(2)", "onepiece(3)", "onepiece(4)"};
        String[] names = {"onepiece", "onepiece(1)", "onepiece(2)", "onepiece(3)", "onepiece"};
        assertArrayEquals(expected, solution.rename(names), "英文测试03");
    }


    @Test
    @DisplayName("Rename 04")
    void rename_04() {
        Solution solution = new Solution();
        String[] expected = {"wano", "wano(1)", "wano(2)", "wano(3)"};
        String[] names = {"wano", "wano", "wano", "wano"};
        assertArrayEquals(expected, solution.rename(names), "英文测试04");
    }

    @Test
    @DisplayName("Rename 05")
    void rename_05() {
        Solution solution = new Solution();
        String[] expected = {"kaido", "kaido(1)", "kaido(2)", "kaido(3)"};
        String[] names = {"kaido", "kaido(1)", "kaido", "kaido(1)"};
        assertArrayEquals(expected, solution.rename(names), "英文测试05");
    }


    @Test
    @DisplayName("Rename 06")
    void rename_06() {
        Solution solution = new Solution();
        String[] expected = {"笔记", "笔记(2)", "笔记(1)", "笔记(3)"};
        String[] names = {"笔记", "笔记(2)", "笔记", "笔记"};
        assertArrayEquals(expected, solution.rename(names), "中文测试06");
    }


    @Test
    @DisplayName("Rename 07")
    void rename_07() {
        Solution solution = new Solution();
        String[] expected = {"笔记", "笔记(2)", "笔记(1)", "笔记(7)", "笔记(2020)", "笔记(3)"};
        String[] names = {"笔记", "笔记(2)", "笔记", "笔记(7)", "笔记(2020)", "笔记",};
        assertArrayEquals(expected, solution.rename(names), "中文测试07");
    }

    @Test
    @DisplayName("Rename 08")
    void rename_08() {
        Solution solution = new Solution();
        String[] expected = {"art", "art(2)", "art(1)", "art(3)", "art(4)", "art(7)", "art(2020)", "art(5)"};
        String[] names = {"art", "art(2)", "art", "art(1)(1)", "art(1)(2)", "art(7)", "art(2020)", "art",};
        assertArrayEquals(expected, solution.rename(names), "复现测试08");
    }

}
